import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  StatusBar,
  Platform,
} from 'react-native';
import {Colors} from '@app/theme';

function Screen({children, style, statusBarColor = Colors['PRIMARY']}) {
  return (
    <SafeAreaView style={[styles.screen, style]}>
      <StatusBar backgroundColor={statusBarColor} />
      {children}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    paddingTop: Platform.OS === 'android' ? 0 : 20,
    flex: 1,
    backgroundColor: Colors.SECONDARY,
  },
});

export default Screen;
