import React, {useContext} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {useNetInfo} from '@react-native-community/netinfo';

import {Colors} from '@app/theme';
import {LocalizationContext} from '@app/contexts';

function OfflineNotice(props) {
  const netInfo = useNetInfo();
  const {translations} = useContext(LocalizationContext);

  if (netInfo.type !== 'unknown' && netInfo.isInternetReachable === false)
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{translations.noInternet}</Text>
      </View>
    );

  return null;
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: Colors.BRAND_RED,
    justifyContent: 'center',
    position: 'absolute',
    width: '100%',
    zIndex: 1,
    padding: 10,
  },
  text: {
    color: Colors.WHITE,
  },
});

export default OfflineNotice;
