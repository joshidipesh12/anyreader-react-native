import React from 'react';

import {Icon} from 'react-native-elements';

function AppIcon({
  name,
  size = 40,
  backgroundColor = '#000',
  iconColor = '#fff',
  type = 'material-community',
  style,
  onPress,
  ...otherProps
}) {
  const container = {
    width: size,
    height: size,
    borderRadius: size / 2,
    backgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
  };

  return (
    <Icon
      containerStyle={[container, style]}
      name={name}
      color={iconColor}
      size={size * 0.5}
      underlayColor="transparent"
      type={type}
      onPress={onPress}
      {...otherProps}
    />
  );
}

export default AppIcon;
