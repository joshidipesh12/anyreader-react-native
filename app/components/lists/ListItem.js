import React from 'react';
import {View, StyleSheet, Image, TouchableHighlight, Text} from 'react-native';
import {Icon} from 'react-native-elements';
import Swipeable from 'react-native-gesture-handler/Swipeable';

import {Colors, GlobalStyles} from '@app/theme';

function ListItem({
  title,
  subTitle,
  image,
  IconComponent,
  rightIcon = 'chevron-right',
  rightIconType = 'material-community',
  iconSize = 25,
  OtherComponent,
  chevron,
  onPress,
  renderRightActions,
}) {
  return (
    <Swipeable renderRightActions={renderRightActions}>
      <TouchableHighlight underlayColor={Colors.light} onPress={onPress}>
        <View style={styles.container}>
          {IconComponent}
          {image && <Image style={styles.image} source={image} />}
          <View style={styles.detailsContainer}>
            <Text style={[GlobalStyles.text, styles.title]} numberOfLines={1}>
              {title}
            </Text>
            {subTitle && (
              <Text style={styles.subTitle} numberOfLines={2}>
                {subTitle}
              </Text>
            )}
          </View>
          {OtherComponent}
          {chevron && (
            <Icon
              color={Colors.medium}
              name={rightIcon}
              type={rightIconType}
              size={iconSize}
            />
          )}
        </View>
      </TouchableHighlight>
    </Swipeable>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: 15,
    backgroundColor: Colors.WHITE,
  },
  detailsContainer: {
    flex: 1,
    marginLeft: 10,
    justifyContent: 'center',
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  subTitle: {
    color: Colors.medium,
  },
  title: {
    fontWeight: '500',
  },
});

export default ListItem;
