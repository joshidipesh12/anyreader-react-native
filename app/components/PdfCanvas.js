import React, {useEffect, useRef} from 'react';
import {ActivityIndicator, StyleSheet, useWindowDimensions} from 'react-native';
import Pdf from 'react-native-pdf';

const PdfCanvas = ({style, filePath = '', page = 1}) => {
  const pdfBox = useRef();
  useEffect(() => {
    pdfBox.current.setPage(page);
    return () => {};
  }, [pdfBox]);
  const window = useWindowDimensions();
  return (
    <Pdf
      ref={pdfBox}
      source={{uri: filePath}}
      onLoadComplete={(numberOfPages, filePath) => {
        //console.log({filePath,numberOfPages})
      }}
      onPageChanged={(page, numberOfPages) => {}}
      onError={(error) => {
        console.log({error});
      }}
      activityIndicator={<ActivityIndicator color="blue" size="large" />}
      onPressLink={(uri) => {}}
      style={[
        styles.pdf,
        style,
        {
          width: window.width,
          height: window.height,
        },
      ]}
    />
  );
};

export default PdfCanvas;

const styles = StyleSheet.create({
  pdf: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
});
