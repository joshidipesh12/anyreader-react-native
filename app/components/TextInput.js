import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import {Colors, GlobalStyles} from '@app/theme';

export default function AppTextInput({
  inputRef,
  width = '100%',
  placeholder,
  keyboardType,
  returnKeyType,
  maxLength,
  forwardRef,
  onChangeText,
  inputContainerStyle,
  textStyle,
  ...otherProps
}) {
  return (
    <View style={[styles.inputContainer, inputContainerStyle, {width: width}]}>
      <TextInput
        ref={inputRef}
        maxLength={maxLength}
        keyboardType={keyboardType}
        placeholder={placeholder}
        returnKeyType={returnKeyType}
        onChangeText={onChangeText}
        style={[GlobalStyles.text, textStyle]}
        {...otherProps}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: Colors.light,
    //width: '90%',
    //alignSelf: 'center',
    //padding: 15,
    borderRadius: 15,
    marginVertical: 10,
    paddingHorizontal: 10,
  },
});
