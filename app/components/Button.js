import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import {Colors, GlobalStyles} from '@app/theme';

function AppButton({
  title,
  titleStyle,
  IconComponent,
  onPress,
  style,
  loading,
  color = 'PRIMARY',
}) {
  return (
    <TouchableOpacity
      style={[styles.button, style, {backgroundColor: Colors[color]}]}
      onPress={onPress}>
      {IconComponent}
      {loading ? (
        <ActivityIndicator size="small" color={Colors.WHITE} />
      ) : (
        <Text style={[GlobalStyles.text, styles.text, titleStyle]}>
          {title}
        </Text>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.primary,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  text: {
    color: Colors.WHITE,
  },
});

export default AppButton;
