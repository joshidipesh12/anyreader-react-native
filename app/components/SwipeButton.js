import React, {useRef} from 'react';
import {
  StyleSheet,
  Text,
  PanResponder,
  Animated,
  SafeAreaView,
} from 'react-native';
import {Colors} from '@app/theme';
import {Icon} from 'react-native-elements';

const SwipeButton = ({navigation}) => {
  const pan = useRef(new Animated.ValueXY()).current;

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        pan.setOffset({
          x: pan.x._value,
          y: pan.y._value,
        });
      },
      onPanResponderMove: Animated.event([
        null,
        {
          dx: pan.x,
          dy: new Animated.Value(0),
        },
      ]),
      onPanResponderRelease: (_, gesture) => {
        if (gesture.moveX < 350) {
          Animated.spring(pan, {
            toValue: {x: 0, y: 0},
            useNativeDriver: false,
          }).start();
        } else {
          pan.flattenOffset();
          navigation.navigate('CheckIn');
        }
      },
    }),
  ).current;

  return (
    <SafeAreaView style={styles.swipeContainer}>
      <Text style={styles.swipeAreaText}>Swipe To Check-Out</Text>
      <Animated.View
        style={[
          styles.swipeThumb,
          {
            transform: [
              {
                translateX: pan.x.interpolate({
                  inputRange: [-10, 300],
                  outputRange: [-10, 300],
                  extrapolateLeft: 'clamp',
                  extrapolateRight: 'clamp',
                }),
              },
              {translateY: pan.y},
            ],
          },
        ]}
        {...panResponder.panHandlers}>
        <Icon name="car" type="material-community" color="white" size={30} />
      </Animated.View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  swipeContainer: {
    alignSelf: 'center',
    position: 'relative',
    width: '85%',
    maxHeight: 100,
    marginVertical: 90,
    borderRadius: 200,
    backgroundColor: Colors.light,
    justifyContent: 'center',
  },
  swipeAreaText: {
    alignSelf: 'center',
    color: Colors.SECONDARY,
    margin: 15,
  },
  swipeThumb: {
    position: 'absolute',
    height: '130%',
    width: '20%',
    backgroundColor: Colors.PRIMARY,
    borderRadius: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SwipeButton;
