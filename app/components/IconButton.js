import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import {Colors} from '@app/theme';
export default function IconButton({
  name,
  type,
  onPress,
  color = Colors.WHITE,
  style,
}) {
  return (
    <TouchableOpacity style={[styles.button, style]} onPress={onPress}>
      <Icon name={name} type={type} color={color} size={20} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    width: 50,
    height: 50,
    borderRadius: 14,
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY,
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: {height: 1, width: 1}, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
});
