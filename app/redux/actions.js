// add pdf
// remove pdf

const addPdf = (pdf) => {
  return {
    type: 'add-pdf',
    payload: pdf,
  };
};

const removePdf = (pdf) => {
  return {
    type: 'remove-pdf',
    payload: pdf,
  };
};

const emptyState = () => {
  return {
    type: 'empty-state',
  };
};

export {addPdf, removePdf, emptyState};
