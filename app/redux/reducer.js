export const pdfReducer = (state = [], action) => {
  switch (action.type) {
    case 'add-pdf':
      return action.payload;
    case 'remove-pdf':
      return state.filter((item) => item.path !== action.payload.path);
    case 'empty-state':
      return [];
    default:
      return state;
  }
};
