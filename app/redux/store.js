import {createStore} from 'redux';
import {pdfReducer} from './reducer';

export let pdfStore = createStore(pdfReducer);
