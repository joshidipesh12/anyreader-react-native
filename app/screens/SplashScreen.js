import React, {useEffect, useContext} from 'react';
import {View, StyleSheet, Image, StatusBar} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {LocalizationContext} from '@app/contexts';
import {get, store} from '../utils/cache';

const SplashDisplay = ({navigation}) => {
  const {initializeAppLanguage} = useContext(LocalizationContext);

  useEffect(() => {
    const timer = setTimeout(async () => {
      initializeAppLanguage();
      SplashScreen.hide();
      let result;
      try {
        result = await get('initial-run-complete');
      } catch (error) {
        console.log(error);
      }
      navigation.navigate(result ? 'Main' : 'Intro');
    }, 0);

    return () => clearTimeout(timer);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image
        style={styles.backgroundContainer}
        source={require('../assets/images/splashscreen_background.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000000',
  },
  backgroundContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
});

export default SplashDisplay;
