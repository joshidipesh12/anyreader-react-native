export {default as SplashScreen} from './SplashScreen';
export {default as FeedbackScreen} from './FeedbackScreen';
export {default as MainScreen} from './MainScreen';
export {default as IntroScreen} from './IntroScreen';
export {default as AboutScreen} from './AboutScreen';
export {default as PdfScreen} from './PdfScreen';
