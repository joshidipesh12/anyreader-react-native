import React, {Component} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Animated,
  Easing,
  BackHandler,
  Alert,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import {Icon} from 'react-native-elements';
import IntroBulbs from '@app/assets/images/intro_bulbs.svg';
import BookReader from '@app/assets/images/BookReader.svg';
import {store, get} from '../utils/cache';

const requestStoragePermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Cool Photo App Storage Permission',
        message: 'Storage Permission Required To Locate Files. ',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
  }
};

export default class IntroScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opacityBulbs: new Animated.Value(0),
      opacityReader: new Animated.Value(0),
      buttonElevation: new Animated.Value(0),
    };

    this.animateButton = () => {
      Animated.spring(this.state.buttonElevation, {
        toValue: 20,
        useNativeDriver: false,
        speed: 2,
      }).start();
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    Animated.timing(this.state.opacityBulbs, {
      toValue: 1,
      duration: 1000,
      delay: 500,
      useNativeDriver: true,
      easing: Easing.ease,
    }).start(
      Animated.timing(this.state.opacityReader, {
        duration: 1000,
        delay: 500,
        toValue: 1,
        useNativeDriver: true,
        easing: Easing.exp,
      }).start(() => this.animateButton()),
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    Alert.alert('Hold on!', 'Are you sure you want to Exit?', [
      {text: 'No', style: 'cancel'},
      {text: 'Yes', style: 'OK', onPress: () => BackHandler.exitApp()},
    ]);
    return true;
  };

  handlePress = async () => {
    try {
      perm = await requestStoragePermission();
      if (perm) {
        await store('initial-run-complete', true);
        this.props.navigation.navigate('Main');
      } else {
        Alert.alert(
          'Sorry!',
          "App Won't Function Properly Without Adequate Permissions",
          [
            {
              text: 'Try Again',
              style: 'OK',
              onPress: () => this.handlePress(),
            },
            {
              text: 'Exit',
              style: 'cancel',
              onPress: () => BackHandler.exitApp(),
            },
          ],
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={[styles.bulbs, {opacity: this.state.opacityBulbs}]}>
          <IntroBulbs />
        </Animated.View>
        <Animated.View
          style={[styles.reader, {opacity: this.state.opacityReader}]}>
          <BookReader />
        </Animated.View>
        <View>
          <Text style={styles.h1}>Welcome To Any Reader</Text>
          <Text style={styles.h3}>The Reader with everything you need.</Text>
        </View>
        <TouchableOpacity onPress={this.handlePress}>
          <Animated.View
            style={[styles.button, {elevation: this.state.buttonElevation}]}>
            <Text allowFontScaling={false} style={styles.h2}>
              Get Started
            </Text>
            <Icon
              name="arrow-right"
              type="material-community"
              size={30}
              color="#ffffff"
            />
          </Animated.View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bulbs: {
    position: 'absolute',
    right: 10,
    top: 0,
  },
  reader: {
    position: 'absolute',
    left: 0,
    bottom: 0,
  },
  h1: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
    textAlign: 'center',
  },
  h2: {
    fontSize: 22,
    textAlign: 'center',
    color: '#fff',
  },
  h3: {
    textAlign: 'center',
    fontSize: 15,
    marginBottom: 30,
  },
  button: {
    backgroundColor: '#074996',
    flexDirection: 'row',
    borderRadius: 50,
    padding: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    minWidth: '50%',
  },
});
