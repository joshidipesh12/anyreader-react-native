import React, {useEffect, useState} from 'react';
import {
  Button,
  Platform,
  StyleSheet,
  Text,
  View,
  useWindowDimensions,
} from 'react-native';
import {PdfCanvas} from '@app/components';
import {Colors} from '@app/theme';

export default function PdfView({route, navigation}) {
  const window = useWindowDimensions();
  const {pdfPath} = route.params;
  const prefix = Platform.OS == 'android' ? 'file://' : null;
  const [pgNum, setPgNum] = useState(1);

  return (
    <View style={[styles.container]}>
      <PdfCanvas
        style={[styles.pdfView, {height: window.height, width: window.width}]}
        filePath={`${prefix}${pdfPath}`}
        onLoadComplete={(numberOfPages, filePath) => {}}
        onPageChanged={(page, numberOfPages) => {}}
      />
      <View style={styles.pageNum}>
        <Text>{pgNum}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pdfView: {
    backgroundColor: Colors.WHITE,
    flex: 1,
  },
  pageNum: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    height: 40,
    width: 40,
    borderRadius: 500,
    backgroundColor: Colors.WHITE,
    elevation: 5,
    bottom: 5,
    left: 5,
  },
});

// route.params.filePath = 'http://www.africau.edu/images/default/sample.pdf'
//const source = { uri: 'http://www.africau.edu/images/default/sample.pdf'};
//const source = require('./test.pdf');  // ios only
//const source = {uri:'bundle-assets://test.pdf'};
//const source = {uri:"data:application/pdf;base64,JVBERi0xLjcKJc..."};
