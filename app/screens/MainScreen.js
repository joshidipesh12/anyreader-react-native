import React, {useState, useLayoutEffect, useRef, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Alert,
  BackHandler,
  Keyboard,
  TouchableWithoutFeedback,
  ToastAndroid,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {Icon, Input, Button} from 'react-native-elements';
import {Colors} from '@app/theme';
import {pdfScan} from '@app/helpers';
import {addPdf} from '@app/redux';

export default function MainScreen({navigation}) {
  const dispatch = useDispatch();
  const pdfData = useSelector((state) => state);
  const [loading, setLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const search = useRef();
  // const [animator, setAnimator] = useState(new Animated.Value(0));

  var displayData = pdfData.filter((item) =>
    item.name.toLowerCase().includes(searchQuery.toLowerCase()),
  );

  const handlePressReload = async () => {
    setLoading(true);
    let data;
    try {
      data = await pdfScan();
    } catch (error) {
      console.log(error);
    }
    dispatch(addPdf([...data]));
    setLoading(false);
  };

  const handlePressAdd = async () => {
    ToastAndroid.show('Coming Soon 😉', ToastAndroid.SHORT);
  };

  useEffect(() => {
    let unsubscribeRemove = navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      Alert.alert('Hold on!', 'Are you sure you want to Exit?', [
        {text: 'No', style: 'cancel'},
        {text: 'Yes', style: 'OK', onPress: () => BackHandler.exitApp()},
      ]);
    });

    let unsubscribeFocus = navigation.addListener('focus', () => {
      handlePressReload();
    });
    return unsubscribeRemove, unsubscribeFocus;
  }, [navigation]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => null,
      // <TouchableOpacity style={styles.headerButton} onPress={() => {}}>
      //   <Icon
      //     name="menu"
      //     type="material-community"
      //     size={25}
      //     color={Colors.WHITE}
      //   />
      // </TouchableOpacity>
    });
  }, [navigation]);

  const renderListItem = ({item}) => (
    <TouchableOpacity
      style={styles.listItem}
      onPress={() => {
        navigation.navigate('PdfScreen', {
          pdfPath: item.path,
          title: item.name,
        });
      }}>
      <View style={{flexDirection: 'row'}}>
        <View>
          <Icon
            name="file-pdf-outline"
            type="material-community"
            size={105}
            color={Colors.CLASSY_RED}
          />
        </View>
        <View style={{flexDirection: 'column', width: '60%'}}>
          <Text
            style={{
              fontFamily: 'OpenSans-Regular',
              fontSize: 20,
              marginTop: 15,
            }}
            numberOfLines={1}>
            {item.name}
          </Text>
          <Text
            style={{
              fontFamily: 'OpenSans-Regular',
              fontSize: 15,
              marginVertical: 5,
            }}
            numberOfLines={1}>
            {`${(item.size / 1024 / 1024)?.toFixed(3)} Mb`}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            position: 'absolute',
            right: 0,
            height: '100%',
            justifyContent: 'center',
            padding: 10,
          }}>
          <Icon
            type="material-community"
            name="dots-vertical"
            color={Colors.SECONDARY}
            size={30}
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={[styles.container]}>
        <Input
          ref={search}
          style={styles.search}
          placeholder="Search For Your PDF"
          onChangeText={(text) => setSearchQuery(text)}
          rightIcon={
            searchQuery ? (
              <TouchableOpacity
                onPress={() => {
                  search.current.clear();
                  setSearchQuery('');
                }}>
                <Icon
                  type="material-community"
                  name="close-circle-outline"
                  size={24}
                  color={Colors.PRIMARY}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  search.current.focus();
                }}>
                <Icon
                  type="material-community"
                  name="magnify"
                  size={24}
                  color={Colors.PRIMARY}
                />
              </TouchableOpacity>
            )
          }
        />
        <FlatList
          data={displayData}
          renderItem={(item) => renderListItem(item)}
          keyExtractor={(item) => item.path}
          ListEmptyComponent={ListEmptyComp}
          showsVerticalScrollIndicator={false}
          refreshing={loading}
          onRefresh={handlePressReload}
          onScroll={() => Keyboard.dismiss()}
        />
        <TouchableOpacity
          onPress={handlePressAdd}
          style={[styles.button, {bottom: 0}]}>
          <Icon type="material-community" name="plus" size={35} color="white" />
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );
}

const ListEmptyComp = () => {
  useEffect(() => {
    return () => {};
  }, []);
  return (
    <View style={{marginTop: 90}}>
      <Text allowFontScaling={false} style={styles.mainText}>
        Its Never Too Late To Choose
      </Text>
      <Text allowFontScaling={false} style={styles.mainText}>
        YOURSELF
      </Text>
      <Image
        style={styles.listEmpty}
        source={require('../assets/images/ListEmpty.png')}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
  },
  headerButton: {
    borderRadius: 50,
    padding: 5,
    margin: 10,
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    paddingHorizontal: 20,
  },
  mainText: {
    textAlign: 'center',
    fontSize: 22,
    marginBottom: 20,
    fontFamily: 'OpenSans-Italic',
  },
  listEmpty: {
    width: 390,
    height: 350,
  },
  button: {
    flexDirection: 'row',
    borderRadius: 50,
    backgroundColor: Colors.PRIMARY,
    padding: 15,
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
    margin: 20,
    elevation: 5,
    height: 65,
    width: 65,
  },
  listItem: {
    backgroundColor: 'white',
    margin: '2%',
    width: '96%',
    elevation: 5,
    borderRadius: 5,
  },
  search: {
    marginTop: 5,
    marginBottom: 0,
  },
});
