import RNFS from 'react-native-fs';
import {Platform} from 'react-native';

const rootDirPath =
  Platform.OS == 'android' ? '/storage/emulated/0' : RNFS.MainBundlePath;

const isType = (file, typeStr = '') => {
  const fileName = file.name.split('.');
  if (fileName[fileName.length - 1].toLowerCase() == typeStr.toLowerCase()) {
    return true;
  } else {
    return false;
  }
};

const pdfScan = async (path = rootDirPath) => {
  let result;
  try {
    result = await RNFS.readDir(path + '/Download');
  } catch (e) {}
  var files = [];
  if (result) {
    for (var item of result) {
      if (isType(item, 'pdf') && item.isFile()) {
        files.push(item);
      }
    }
  }
  return await Promise.resolve(files);
};

export default pdfScan;
