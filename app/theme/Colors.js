export default {
  PRIMARY: '#074996',
  PRIMARYDARK: '#012E62',
  ACCENT: '#FF4081',
  GREY: '#333333',
  SUGAR_CANE: '#EFF0E2',
  WHITE: '#FFFFFF',
  LOGO_COLOR: '#A21F35',
  BLACK: '#000000',
  ACTIVE: '#2A8947',
  BLUE: '#006774',
  RED: '#961730',
  GREEN: '#93C01F',
  orange: '#F6A610',
  DISABLED_GREY: '#b4b4b4',
  DISABLED_GREYDARK: '#727272',
  BUTTON_DISABLED: '#006774',
  BUTTON_ENABLED: '#427D9C',
  BUTTON_RED: '#e13c47',
  NEXT_WAYPOINT: '#d6e3e9',
  LATE: '#f06f59',
  EARLY: '#ecbcf0',
  UNDEFINED: '#427d9c',
  IN_JEOPARDY: '#fbb73e',
  ON_TIME: '#93d498',
  HEADER: '#d6e3e9',
  BRAND_RED: '#DE292F',
  BRAND_BLUE: '#184273',
  BRAND_DARK_RED: '#B01A27',
  VELVET_RED: '#86242a',
  CLASSY_RED: '#c9003c',
  BORDER_COLOR: '#8A8D90',
  medium: '#6e6969',
  light: '#f8f4f4',
  dark: '#0c0c0c',
  danger: '#ff5252',
  SECONDARY: '#0094FF',
};
