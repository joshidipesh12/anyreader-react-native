import {StyleSheet, Dimensions} from 'react-native';
import Colors from './Colors';

const {height} = Dimensions.get('window');

const GlobalStyles = StyleSheet.create({
  text: {
    color: Colors.dark,
    fontSize: height > 700 ? 18 : 16,
    fontFamily: Platform.OS === 'android' ? 'Roboto' : 'Avenir',
  },
});

export default GlobalStyles;
