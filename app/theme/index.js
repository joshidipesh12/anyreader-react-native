import Colors from './Colors';
import {FontWeights, FontSizes} from './Typography';
import GlobalStyles from './Styles';
export {Colors, FontWeights, FontSizes, GlobalStyles};
