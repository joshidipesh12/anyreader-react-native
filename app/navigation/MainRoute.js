import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {SplashScreen, MainScreen, IntroScreen, PdfScreen} from '@app/screens';
import {LocalizationContext} from '@app/contexts';
import {Colors} from '@app/theme';

const Stack = createStackNavigator();

export default function MainRoute() {
  const {translations} = useContext(LocalizationContext);
  return (
    <Stack.Navigator initialRouteName="Splash" mode="modal" headerMode="screen">
      <Stack.Screen
        name="Splash"
        component={SplashScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Intro"
        component={IntroScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Main"
        component={MainScreen}
        options={{
          headerShown: false,
          headerTintColor: Colors.WHITE,
          headerStyle: {
            backgroundColor: Colors.PRIMARY,
          },
          headerTitle: 'Any Reader',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-BoldItalic',
          },
        }}
      />
      <Stack.Screen
        name="PdfScreen"
        component={PdfScreen}
        options={({route}) => ({
          title: route.params.title,
          headerStyle: {
            backgroundColor: Colors.PRIMARY,
          },
          headerTintColor: Colors.WHITE,
          headerTitleStyle: {
            fontFamily: 'OpenSans-BoldItalic',
          },
        })}
      />
    </Stack.Navigator>
  );
}
