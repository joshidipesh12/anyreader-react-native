import AsyncStorage from '@react-native-async-storage/async-storage';
import dayjs from 'dayjs';

const prefix = 'cache';
const expiryInMinutes = 5;

const store = async (key, value) => {
  try {
    const item = {
      value,
      timestamp: Date.now(),
    };
    await AsyncStorage.setItem(prefix + key, JSON.stringify(item));
  } catch (error) {
    console.log(error);
  }
};

const isExpired = (item) => {
  const now = dayjs();
  const storedTime = dayjs(item.timestamp);
  return now.diff(storedTime, 'minute') > expiryInMinutes;
};

const get = async (key) => {
  let value;
  try {
    value = await AsyncStorage.getItem(prefix + key);

    /*     if (isExpired(item)) {
      // Command Query Separation (CQS)
      await AsyncStorage.removeItem(prefix + key);
      return null;
    } */
  } catch (error) {
    console.log(error);
    return null;
  }
  const item = JSON.parse(value);
  if (!item) {
    return null;
  }
  return item.value;
};

const remove = async (key) => {
  await AsyncStorage.removeItem(prefix + key);
  return null;
};

export {store, get, remove};
