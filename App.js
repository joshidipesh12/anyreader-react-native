import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {LocalizationProvider} from '@app/contexts';
import {MainRoute} from '@app/navigation';
import {pdfStore} from '@app/redux';

const App = () => {
  return (
    <Provider store={pdfStore}>
      <LocalizationProvider>
        <NavigationContainer>
          <MainRoute />
        </NavigationContainer>
      </LocalizationProvider>
    </Provider>
  );
};

export default App;
