# AnyReader - React Native :fire:

An all-in-one reader implimentation for pdf, epub, word, txt file types in React Native...with basic [Redux](https://redux.js.org/). :rocket:

# ScreenShots

![ScreenShot1](./Screenshots/splash-screen.png) ![ScreenShot2](./Screenshots/MainScreen.png )![ScreenShot3](./Screenshots/IntroScreen.png )

# Features
### Working
- Splash Screen
- Basic Navigation
- Loading PDFs from Downloads folder(For Now)
- Reloading PDF everytime List Appears
- Searching files that are listed

### On The Way
1. PDF Autoload from all the folders
2. Drawer
3. Adding PDF manually
