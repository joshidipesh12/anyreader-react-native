module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: ['syntax-dynamic-import', '@babel/plugin-syntax-dynamic-import'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          '@app/src': './src',
          '@app/navigation': './app/navigation',
          '@app/screens': './app/screens',
          '@app/theme': './app/theme',
          '@app/utils': './app/utils',
          '@app/redux': './app/redux',
          '@app/contexts': './app/contexts',
          '@app/components': './app/components',
          '@app/constants': './app/constants',
          '@app/translations': './app/translations',
          '@app/api': './app/api',
          '@app/hooks': './app/hooks',
          '@app/helpers': './app/helpers',
          '@app/assets': './app/assets',
        },
      },
    ],
  ],
};
